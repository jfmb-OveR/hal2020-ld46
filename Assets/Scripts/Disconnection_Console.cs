﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Disconnection_Console : MonoBehaviour
{
    [SerializeField]
    MyGameController myGame;

    public void StartCountdown(){
        myGame.SetCounterDown(true);
    }
}
