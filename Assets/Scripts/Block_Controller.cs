﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Block_Controller : MonoBehaviour
{
    [SerializeField]
    int iID;
    [SerializeField]
    Puzzle_Controller myPuzzleController;
    [SerializeField]
    string sAnimatorString;
    Animator myAnimator;
    int iAnimatorHash;

    public void EnableDisableBlock(bool bNewValue){
        myAnimator.SetBool(iAnimatorHash, bNewValue);
    }

    public void OnMouseDown(){
//        if(myPuzzleController.GetAllowTouchingBlocksFromGameController() == true){
            Debug.Log("He hecho click");
            myPuzzleController.CheckCombination(iID);
//        }
    }

    // Start is called before the first frame update
    void Start()
    {
        myAnimator = this.GetComponent<Animator>();
        iAnimatorHash = Animator.StringToHash(sAnimatorString);
        EnableDisableBlock(false);
    }
}
