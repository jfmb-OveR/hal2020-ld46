﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Puzzle_Controller : MonoBehaviour
{
    [SerializeField]
    MyGameController myGame;
    [SerializeField]
    private int[] arrayIntCombination;
    [SerializeField]
    private GameObject[] arrayGoBlocks;
    [SerializeField]
    AudioSource audioFail;
    
    int iCounter = 0;

    public bool GetAllowTouchingBlocksFromGameController(){
        return myGame.GetAllowTouchingBlocks();
    }

    public void ResetBlocks(){
        for (int i = 0; i < arrayGoBlocks.Length; i++){
            arrayGoBlocks[i].GetComponent<Block_Controller>().EnableDisableBlock(false);
        }
        iCounter = 0;
    }

    public void CheckCombination(int iNewValue){
        if(myGame.GetEndGame() == false){
            if(arrayIntCombination[iCounter] == iNewValue){
                //Activar animación de bloque
                arrayGoBlocks[iNewValue].GetComponent<Block_Controller>().EnableDisableBlock(true);
                iCounter++;
                if(iCounter >= arrayIntCombination.Length)
                    myGame.EndGame(3);
            }
            else{
                audioFail.Play();
                ResetBlocks();
            }
        }
    }
}
