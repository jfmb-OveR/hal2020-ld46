﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Hal_Lines : MonoBehaviour
{
    // Start is called before the first frame update{
//    [SerializeField]
//    GameObject goSound;
//    [SerializeField]
//    MyGameController myGame;

    [SerializeField]
    string sString;

    Text tTitle;

    private int iIndex = 0;

    IEnumerator CorWriteText(char cCharacter){
        tTitle.text = tTitle.text + cCharacter;

        iIndex++;
        if(iIndex < sString.Length){
            yield return new WaitForSeconds(.1f);
            StartCoroutine(CorWriteText(sString[iIndex]));
        }
        else{
            yield return new WaitForSeconds(2f);
//            goSound.SetActive(false);
            iIndex = 0;
            tTitle.text = "";
            yield return null;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        iIndex = 0;
        tTitle = this.GetComponent<Text>();
        tTitle.text = "";

//        goSound.SetActive(true);
        if(sString.Length > 0)
            StartCoroutine(CorWriteText(sString[iIndex]));
    }
}
