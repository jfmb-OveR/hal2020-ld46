﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Title_Script : MonoBehaviour
{
    [SerializeField]
    GameObject goSoundKeyboard;
    [SerializeField]
    GameObject goSoundKeyboard02;
    [SerializeField]
    GameObject goDoorOpenning;
    [SerializeField]
    MyGameController myGame;
    [SerializeField]
    GameObject goTextAccessGranted;

    [SerializeField]
    string sTitle = "HAL 2020 \r\n\n a game by \r\n\n Over Software";
    string sSecondString = "Get access to: \r\n\n Hal's room \r\n\n......";
    [SerializeField]
    float fTimeToStart;

    Text tTitle;

    private int iIndex = 0;

    IEnumerator CorStartTitle(){
        yield return new WaitForSeconds(fTimeToStart);

        goSoundKeyboard.SetActive(true);
        StartCoroutine(CorWriteTitle(sTitle[iIndex]));
    }

    IEnumerator CorWriteTitle(char cCharacter){
        tTitle.text = tTitle.text + cCharacter;

        iIndex++;
        if(iIndex < sTitle.Length){
            yield return new WaitForSeconds(.1f);
            StartCoroutine(CorWriteTitle(sTitle[iIndex]));
        }
        else{
            yield return new WaitForSeconds(2f);
            goSoundKeyboard.SetActive(false);
            iIndex = 0;
            tTitle.text = "";
            StartCoroutine(CorWriteSecondString(sSecondString[iIndex]));
        }
    }


    IEnumerator CorWriteSecondString(char cCharacter){
        tTitle.text = tTitle.text + cCharacter;
        goSoundKeyboard02.SetActive(true);

        iIndex++;
        if(iIndex < sSecondString.Length){
            yield return new WaitForSeconds(.1f);
            StartCoroutine(CorWriteSecondString(sSecondString[iIndex]));
        }
        else{
            yield return new WaitForSeconds(2f);
            iIndex = 0;
            goTextAccessGranted.SetActive(true);
            goSoundKeyboard02.SetActive(false);
            goDoorOpenning.SetActive(true);

            StartCoroutine(CorStartGame());
        }
    }

    IEnumerator CorStartGame(){
        yield return new WaitForSeconds(5f);
        myGame.EndTitleStartGame();
        yield return null;
    }

    // Start is called before the first frame update
    void Awake()
    {
        iIndex = 0;
        goSoundKeyboard.SetActive(false);
        goSoundKeyboard02.SetActive(false);
        goDoorOpenning.SetActive(false);

        tTitle = this.GetComponent<Text>();
        tTitle.text = "";
        Debug.Log("Texto: " + sTitle);

        goTextAccessGranted.SetActive(false);

        StartCoroutine(CorStartTitle());
//        StartCoroutine(CorWriteTitle(sTitle[iIndex]));
    }

    // Update is called once per frame
}
