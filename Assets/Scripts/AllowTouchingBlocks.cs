﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AllowTouchingBlocks : MonoBehaviour
{
    [SerializeField]
    MyGameController myGame;
    public void EnableTouchingBlocks(){
        myGame.SetAllowTouchingBlocks(true);
    }

    public void FadeOut(){
        myGame.FadeOut();
    }

}
