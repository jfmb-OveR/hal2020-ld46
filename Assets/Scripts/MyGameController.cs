﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MyGameController : MonoBehaviour
{
    [SerializeField]
    GameObject goFadeIn;
    [SerializeField]
    GameObject goFadeOut;

    [SerializeField]
    GameObject goTitleObjects;

    [SerializeField]
    GameObject goSceneObjects;
//    [SerializeField]
//    GameObject goFrontPanel;
    [SerializeField]
    GameObject goEndObjects;

    [SerializeField]
    GameObject goHalAlive;
    [SerializeField]
    GameObject goHalDead;

    [SerializeField]
    GameObject goHalTexts;

    [SerializeField]
    GameObject goHalTextCounterDown;
    [SerializeField]
    GameObject goHalTextEnd01;
    [SerializeField]
    GameObject goHalTextEnd02;
    [SerializeField]
    GameObject goHalTextEnd03;
    [SerializeField]
    GameObject goSoundHalDie;
    [SerializeField]
    GameObject goSoundAlarm;


    [SerializeField]
    Text textCounter;
    [SerializeField]
    GameObject goAmbienceSoundSpaceShip;
    [SerializeField]
    GameObject goSoundBreathing;
    private bool bEndGame = false;

    private bool bAllowTouchingBlocks = false;

    private bool bCounterDownEnabled = false;

    private bool bHalIsAlive = true;

    float fTimer = 60f;
    int iTimerInteger = 0;
    public bool GetEndGame(){
        return bEndGame;
    }

    public bool GetAllowTouchingBlocks(){
        return bAllowTouchingBlocks;
    }

    public void SetAllowTouchingBlocks(bool bNewValue){
        bAllowTouchingBlocks = bNewValue;
    }

    public void SetCounterDown(bool bNewValue){
        bCounterDownEnabled = bNewValue;
        if(bNewValue == true){
            goHalTexts.SetActive(false);
            goHalTextCounterDown.SetActive(true);
        }
    }

    public void EnableDisableAlarm(bool bNewValue){
        goSoundAlarm.SetActive(bNewValue);
    }

    public void SetHalIsAlive(bool bNewValue){
        bHalIsAlive = bNewValue;
    }

    public void FadeOut(){
        goFadeOut.SetActive(true);
        StartCoroutine(CorFadeOut());
    }
    public void FadeIn(){
        goFadeIn.SetActive(true);
    }

    private void EndGame01(){
        EnableDisableAlarm(false);
        goHalTextEnd01.SetActive(true);
    }

    private void EndGame02(){
        goAmbienceSoundSpaceShip.SetActive(false);
        goSoundBreathing.SetActive(false);
        goHalTextEnd02.SetActive(true);        
    }

    private void EndGame03(){
//        goSoundHalDie.SetActive(true);
        EnableDisableAlarm(false);
        SetCounterDown(false);
        goHalTextEnd03.SetActive(true);  
        SetHalIsAlive(false);
    }

    public void EndGame(int iEnding){
        goHalTexts.SetActive(false);
        goHalTextCounterDown.SetActive(false);
        goHalTextEnd01.SetActive(false);        
        goHalTextEnd02.SetActive(false);        
        goHalTextEnd03.SetActive(false);        

        switch(iEnding){
            case 1:
                EndGame01();
                Debug.Log("Final 1: Dalton cierra el panel y no desconecta a Hal");
            break;

            case 2:
                EndGame02();
                Debug.Log("Final 2: Dalton intenta desconectar a Hal pero no le da tiempo");
            break;

            case 3: 
                EndGame03();
                Debug.Log("Final 3: Dalton desconecta a Hal");
            break;
        }
    }

    public void EndTitleStartGame(){
        goTitleObjects.SetActive(false);
        goSceneObjects.SetActive(true);

        goHalTexts.SetActive(true);

    }

    IEnumerator CorFadeOut(){
        goEndObjects.SetActive(true);
        goHalAlive.SetActive(bHalIsAlive);
        goHalDead.SetActive(!bHalIsAlive);

        goSceneObjects.SetActive(false);
        yield return new WaitForSeconds(3f);
        goFadeOut.SetActive(false);
//        ResetGame();
        FadeIn();
    }

    void ResetGame(){
        goTitleObjects.SetActive(true);
        goSceneObjects.SetActive(false);
//        goFrontPanel.SetActive(false);
        goEndObjects.SetActive(false);

        goHalTexts.SetActive(false);
        goHalTextCounterDown.SetActive(false);
        goHalTextEnd01.SetActive(false);        
        goHalTextEnd02.SetActive(false);        
        goHalTextEnd03.SetActive(false);        

        goFadeIn.SetActive(false);
        goFadeOut.SetActive(false);

        goHalAlive.SetActive(false);
        goHalDead.SetActive(false);

        goSoundHalDie.SetActive(false);

        EnableDisableAlarm(false);

        fTimer = 60f;
        iTimerInteger = (int)fTimer;
    }

    // Start is called before the first frame update
    void Start()
    {
        ResetGame();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape)){
            Application.Quit();
            Debug.Log("Exit game");
        }

        if(bCounterDownEnabled == true){
            fTimer -= Time.deltaTime;
            iTimerInteger = (int)fTimer;

            if(iTimerInteger > 9f)
                textCounter.text = "00:" + iTimerInteger.ToString();
            else
                textCounter.text = "00:0" + iTimerInteger.ToString();

//            Debug.Log( "00:" + iTimerInteger.ToString());
            if(fTimer <= 0){
                bCounterDownEnabled = false;
                EndGame(2);
            }
        }
    }
}
