﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Button_Console : MonoBehaviour
{
    [SerializeField]
    MyGameController myGame;
    [SerializeField]
    Text textButton;
    [SerializeField]
    string sAnimatorString;
    [SerializeField]
    Animator myAnimator;
    int iAnimatorHash;

    bool bButtonPressed = false;
    Vector3 vOriginalScale;
    
    void OnMouseDown(){
        if(bButtonPressed == false){
            this.transform.localScale = new Vector3(1.15f, 0.25f, 0.15f);
            EnableDisableBlock(true);
            textButton.text = "Close console";
            bButtonPressed = true;
            myGame.EnableDisableAlarm(true);
        }
        else{
            EnableDisableBlock(false);
            myGame.EndGame(1);
        }
    }

    void OnMouseUp(){
        this.transform.localScale = new Vector3(vOriginalScale.x, vOriginalScale.y, vOriginalScale.z);
    }

    public void EnableDisableBlock(bool bNewValue){
        myAnimator.SetBool(iAnimatorHash, bNewValue);
    }

    void Start(){
        vOriginalScale = this.transform.localScale;
        iAnimatorHash = Animator.StringToHash(sAnimatorString);
        EnableDisableBlock(false);
    }
}
